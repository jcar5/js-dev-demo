import { TestBed } from '@angular/core/testing';

import { DataDispatcherService } from './data-dispatcher.service';

describe('DataDispatcherService', () => {
  let service: DataDispatcherService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataDispatcherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
