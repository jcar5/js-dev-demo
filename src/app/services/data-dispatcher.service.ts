import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { LocationsActions, PeopleActions } from '../store/actions';
import { PeopleState } from '../store/reducers/people.reducer';

@Injectable({
  providedIn: 'root',
})
export class DataDispatcherService {
  dispatchPeople() {
    return this.store.dispatch(PeopleActions.getPeople());
  }

  dispatchLocations() {
    return this.store.dispatch(LocationsActions.getLocations());
  }

  constructor(private readonly store: Store<PeopleState>) {}
}
