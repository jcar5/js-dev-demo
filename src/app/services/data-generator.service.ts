import { Injectable } from '@angular/core';
import { DMLocation } from '../models/DMLocation';
import { Person } from '../models/Person';

@Injectable({
  providedIn: 'root',
})
export class DataGeneratorService {
  private static readonly people: Person[] = [
    {
      id: 1,
      firstName: 'Jim',
      lastName: 'Halpert',
      locations: [1, 2],
    },
    {
      id: 2,
      firstName: 'Pam',
      lastName: 'Beesley',
      locations: [1, 2],
    },
    {
      id: 3,
      firstName: 'Kelly',
      lastName: 'Kapoor',
      locations: [1],
    },
    {
      id: 4,
      firstName: 'Michael',
      lastName: 'Scott',
      locations: [1, 2],
    },
    {
      id: 5,
      firstName: 'Dwight',
      lastName: 'Schrute',
      locations: [1],
    },
    {
      id: 6,
      firstName: 'Phyllis',
      lastName: 'Vance',
      locations: [1, 3, 4],
    },
    {
      id: 7,
      firstName: 'Oscar',
      lastName: 'Martinez',
      locations: [1, 2, 3],
    },
    {
      id: 8,
      firstName: 'Creed',
      lastName: 'Bratton',
      locations: [2, 4],
    },
    {
      id: 9,
      firstName: 'Darryl',
      lastName: 'Philbin',
      locations: [1, 3],
    },
    {
      id: 10,
      firstName: 'Andy',
      lastName: 'Bernard',
      locations: [2, 4],
    },
  ];

  private static readonly locations: DMLocation[] = [
    {
      id: 1,
      city: 'Scranton',
      state: 'PA',
    },
    {
      id: 2,
      city: 'New York City',
      state: 'NY',
    },
    {
      id: 3,
      city: 'Buffalo',
      state: 'NY',
    },
    {
      id: 4,
      city: 'Akron',
      state: 'OH',
    },
  ];

  constructor() {}

  static objectifyArray(arr: any[]) {
    const obj = {};
    arr.forEach(arrItem => obj[arrItem.id] = arrItem);

    return obj;
  }

  static getPeopleData() {
    return this.objectifyArray(this.people);
  }

  static getLocationData() {
    return this.objectifyArray(this.locations);
  }
}
