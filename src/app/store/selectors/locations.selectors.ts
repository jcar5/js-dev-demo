import { createSelector, createFeatureSelector } from '@ngrx/store';
import { State } from '../reducers';

export const selectState = createFeatureSelector('locations');

export const selectLocations = createSelector(
    selectState,
    (state: State) => {
        return state ? state.locations : null;
    }
);
