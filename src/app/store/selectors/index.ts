import * as LocationsSelectors from './locations.selectors';
import * as PeopleSelectors from './people.selectors';

export { LocationsSelectors, PeopleSelectors };
