import * as PeopleActions from './people.actions';
import * as LocationsActions from './locations.actions';

export { PeopleActions, LocationsActions };
