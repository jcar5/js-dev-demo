import { createAction, props, Action } from '@ngrx/store';
import { DMLocation } from 'src/app/models/DMLocation';

export enum ActionTypes {
  GetLocations = '[Locations API] Get Locations',
  GetLocationsSuccess = '[Locations API] Get Locations Success',
}

export const getLocations = createAction(ActionTypes.GetLocations);

export const getLocationsSuccess = createAction(
  ActionTypes.GetLocationsSuccess,
  props<{ locations: { [key: number]: DMLocation } }>()
);
