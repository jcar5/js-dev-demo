import { createAction, props, Action } from '@ngrx/store';
import { Person } from 'src/app/models/Person';

export enum ActionTypes {
  GetPeople = '[People API] Get People',
  GetPeopleSuccess = '[People API] Get People Success',
}

export const getPeople = createAction(
  ActionTypes.GetPeople
);

export const getPeopleSuccess = createAction(
    ActionTypes.GetPeopleSuccess,
    props<{ people: { [key: number]: Person } }>()
);
