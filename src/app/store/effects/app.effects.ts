import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { DataGeneratorService } from 'src/app/services/data-generator.service';
import { debounceTime, map } from 'rxjs/operators';
import { PeopleActions, LocationsActions } from '../actions';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';


@Injectable()
export class AppEffects {
  constructor(private actions$: Actions) {}

  loadPeople$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(PeopleActions.ActionTypes.GetPeople),
      debounceTime(8000),
      map(_ => PeopleActions.getPeopleSuccess({ people: DataGeneratorService.getPeopleData() }))
    )
  );

  loadLocations$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(LocationsActions.ActionTypes.GetLocations),
      debounceTime(4000),
      map(_ =>
        LocationsActions.getLocationsSuccess({
          locations: DataGeneratorService.getLocationData(),
        })
      )
    )
  );
}
