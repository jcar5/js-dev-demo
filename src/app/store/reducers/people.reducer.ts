import { createReducer, on, Action } from '@ngrx/store';
import { PeopleActions } from '../actions';
import { Person } from '../../models/Person';

export interface PeopleState { [key: number]: Person; }
export const initialPeopleState = null;

const peopleReducer = createReducer(
  initialPeopleState,
  on(PeopleActions.getPeopleSuccess, (state, { people }) => ({
    ...state,
    people,
  }))
);

export function peopleReducerFn(
  state: PeopleState | undefined,
  action: Action
): PeopleState {
  return peopleReducer(state, action);
}
