import {
  ActionReducerMap,
  MetaReducer,
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { LocationsState, initialLocationsState, locationsReducerFn } from './location.reducer';
import { PeopleState, initialPeopleState, peopleReducerFn } from './people.reducer';

export interface State {
  people: PeopleState;
  locations: LocationsState;
}

export const initialState = {
  people: initialPeopleState,
  locations: initialLocationsState
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

export const reducers: ActionReducerMap<State> = {
  people: peopleReducerFn,
  locations: locationsReducerFn,
};
