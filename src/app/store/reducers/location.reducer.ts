import { createReducer, on, Action } from '@ngrx/store';
import { LocationsActions } from '../actions';
import { DMLocation } from 'src/app/models/DMLocation';

export interface LocationsState { [key: number]: DMLocation; }
export const initialLocationsState = null;

const locationsReducer = createReducer(
  initialLocationsState,
  on(LocationsActions.getLocationsSuccess, (state, { locations }) => ({
    ...state,
    locations,
  }))
);

export function locationsReducerFn(
  state: LocationsState | undefined,
  action: Action
): LocationsState {
  return locationsReducer(state, action);
}
