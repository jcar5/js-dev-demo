import { Component, OnInit, ViewChild } from '@angular/core';
import { DMLocation } from 'src/app/models/DMLocation';
import { Person } from 'src/app/models/Person';
import {DataFetcherService} from'src/app/services/data-fetcher.service'
import { Subscription } from 'rxjs';
import { DataDispatcherService } from 'src/app/services/data-dispatcher.service';
import { MatTableDataSource } from '@angular/material/table';
import { Sort } from '@angular/material/sort';

//peopleAdapted turns the locations property into a string of all work locations for an employee
export interface peopleAdapted {
  id: number;
  firstName: string;
  lastName: string;
  locations: string;
}

@Component({
  selector: 'app-location-tables',
  templateUrl: './location-tables.component.html',
  styleUrls: ['./location-tables.component.scss']
})
export class LocationTablesComponent implements OnInit {


  constructor(
    protected dataFetcherService: DataFetcherService,
    protected dataDispatcherService: DataDispatcherService,
  ) { }

  subscriptions: Subscription[]=[];
  
  locations:DMLocation[];
  people:Person[];
  peopleAdaptedArray:peopleAdapted[]=[];

  getLocations=this.dataFetcherService.getLocations();
  getPeople=this.dataFetcherService.getPeople();
  
  //maps a location's id to a string of city and state
  locationMapper={};
  
  dataSource:MatTableDataSource<peopleAdapted> = new MatTableDataSource<peopleAdapted>([]);
  columnsDisplay = ['name','locations'];

  ngOnInit(): void {
    this.generateLocations();
    this.generatePeople();
    this.initObservables();
  }

   initObservables(): void{
    this.observeLocations();
    this.observePeople();
  }

  generateLocations():void{
    this.dataDispatcherService.dispatchLocations();
  }

  generatePeople():void{
    this.dataDispatcherService.dispatchPeople();
  }

  observeLocations(): void{
    this.subscriptions.push(
      this.getLocations.subscribe((locations:DMLocation[]) =>{
       this.locations=locations;
       for (const l in this.locations){
        this.locationMapper[this.locations[l].id]=`${this.locations[l].city}, ${this.locations[l].state}`
       }
      })
    )
  }

  observePeople(): void{
    this.subscriptions.push(
      this.getPeople.subscribe((people:Person[]) =>{
        this.people=people;
        
        //Changed the location ids to to strings for management in the table
        this.people.forEach(p =>{
          let location="";

          p.locations.forEach(l =>{
             location==="" ? location=this.locationMapper[l] : location+="; "+this.locationMapper[l];
          })

          this.peopleAdaptedArray.push({
            id:p.id,
            firstName:p.firstName,
            lastName:p.lastName,
            locations:location

          })
        })

        this.dataSource.data=[...this.peopleAdaptedArray];
      })
    )
  }
  
  // Allows each column to be sorted in ascending or descending
  sortData(sort:Sort){
    const data = [...this.peopleAdaptedArray];

    if (!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a,b) =>{
      const isAsc=sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return this.compare(a.lastName,b.lastName,isAsc);
        case 'locations': return this.compare(a.locations,b.locations,isAsc);
        default: return 0;
      }
    })
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  
  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
    });
  }

}
