import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationTablesComponent } from './location-tables.component';

describe('LocationTablesComponent', () => {
  let component: LocationTablesComponent;
  let fixture: ComponentFixture<LocationTablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationTablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
