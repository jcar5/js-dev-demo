export interface DMLocation {
    id: number;
    city: string;
    state: string;
}
